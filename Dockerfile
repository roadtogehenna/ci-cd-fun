FROM python:3.8.0-slim

WORKDIR /app
COPY . .
RUN pip install --trusted-host pypi.python.org Flask
ENV NAME Mark
EXPOSE 8080
CMD ["python", "app.py"]